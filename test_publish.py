import subprocess
import ssl
import time as t
import json
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT
import AWSIoTPythonSDK.exception.AWSIoTExceptions as AWSExc
# Define ENDPOINT, CLIENT_ID, PATH_TO_CERT, PATH_TO_KEY, PATH_TO_ROOT, MESSAGE, TOPIC, and RANGE
hostname = subprocess.run(["hostname", "-f"], capture_output=True, text=True)
hostname = hostname.stdout.strip()
CLIENT_ID = hostname + "_Core"
TXT_EXT = ".txt"
info_file_path = str(CLIENT_ID) + TXT_EXT
info = ""
with open(info_file_path, "r") as file:
    info = file.read()
    info = json.loads(info)
ENDPOINT = info['iotEndpoint']
PATH_TO_CERT = info['keysName'] + ".pem"
PATH_TO_KEY = info['keysName'] + ".key"
PATH_TO_ROOT =  "root.ca.pem"
MESSAGE = "Questo è un messaggio di test"
TOPIC = "test/testing"
RANGE = 50
try:
    myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
    myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
    myAWSIoTMQTTClient.configureCredentials(PATH_TO_ROOT, PATH_TO_KEY, PATH_TO_CERT)
    #myAWSIoTMQTTClient.configureOfflinePublishingQueueing(-1, AWSIoTPyMQTT.DROP_NEWEST)
    myAWSIoTMQTTClient.connect()
    print('Begin Publish')
    for i in range (RANGE):
        data = "{} [{}]".format(MESSAGE, i+1)
        message = {"message" : data}
        myAWSIoTMQTTClient.publish(TOPIC, json.dumps(message), 1) 
        print("Published: '" + json.dumps(message) + "' to the topic: " + "'test/testing'")
        t.sleep(3)
    print('Publish End')
    myAWSIoTMQTTClient.disconnect()
except ssl.SSLError as e:
    print('Key mismatch')
except AWSExc.connectTimeoutException as timex:
    print(timex.message)
