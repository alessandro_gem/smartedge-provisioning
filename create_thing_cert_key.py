import sys
import time
import os
import subprocess
import boto3
from botocore.exceptions import ClientError
from ggCloudFormation import create_stack
import json
import logging

hostname = subprocess.run(["hostname", "-f"], capture_output=True, text=True)
OUTPUT_PATH = "/home/ggc_user/debug/"
GG_CORE_NAME = str(hostname.stdout.strip()) + "_Core"
KEY_EXT = ".key"
PEM_EXT = ".pem"
TXT_EXT = ".txt"

# set logging on stdout
logger = logging.getLogger()
logger.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
policy_name = "SmartEdge-gg-policy"

          
'''
    Writing output info to file
'''
def write_key_and_cert(m_id, thingName, info):
    try:
        pk_file_path = OUTPUT_PATH + str(m_id) + KEY_EXT
        with open(pk_file_path, "w") as file:
            file.write(str(info['privateKey']))
        certPem_file_path = OUTPUT_PATH + str(m_id) + PEM_EXT
        with open(certPem_file_path, "w") as file:
            file.write(str(info['certificatePem']))
        info_file_path = OUTPUT_PATH + str(thingName) + TXT_EXT
        with open(info_file_path, "w") as file:
            file.write(json.dumps(info))
    except Exception as e:
        print("An error occurred: " + str(e))

'''
    Provisioning function. Creates thing and access policy to the AWS Cloud. It also generates key and certificate on local file system and cloud (for authentication).
'''
def cfn(thingName):
    outputData={}
    try:
        client = boto3.client('iot')
        my_session = boto3.session.Session()
        my_region = my_session.region_name
        thing = client.create_thing(thingName=thingName)
        response = client.create_keys_and_certificate(setAsActive=True)
        certId = response['certificateId']
        certArn = response['certificateArn']
        certPem = response['certificatePem']
        privateKey = response['keyPair']['PrivateKey']
        # publicKey = response['keyPair']['PublicKey']
        policy = client.get_policy(policyName=policy_name)
        if policy:
            print("Policy trovata!\n")
            response = client.attach_policy(policyName=policy_name, target=certArn)
        else:
            print("Policy non trovata")
            exit(0)
        response = client.attach_thing_principal(thingName=thingName, principal=certArn,)
        logger.info('Created thing: %s, cert: %s and policy: %s' % (thingName, certId, policy_name))
        outputData['certificateId'] = certId
        outputData['certificatePem'] = certPem
        outputData['privateKey'] = privateKey
        outputData['iotEndpoint'] = client.describe_endpoint(endpointType='iot:Data-ATS')['endpointAddress']
        outputData['thingName'] = thing['thingName']
        outputData['thingArn'] = thing['thingArn']
        outputData['thingId'] = thing['thingId']
        outputData['awsRegion'] = my_region
        outputData['keysName'] = certId[0:10]
    except ClientError as e:
        logger.error('Error: {}'.format(e.response['Error']['Message']))
    write_key_and_cert(certId[0:10], thingName, outputData)
    sys.stdout.flush()
    return outputData

    
'''
   Greengrass Setup launcher function. When done, reboots the system to apply changes.
'''
def install_greengrass(data):
    try:
        res = subprocess.run(['sh', './greengrass_setup.sh', data['keysName'], data['thingArn'], data['iotEndpoint'], data['awsRegion']], text=True)
        #res.returncode = 0
        if res.returncode == 0:
            #print(res.stdout)
            print("rebooting the system ...")
            time.sleep(1)
            os.system('reboot')
        else:
            print("Comando non eseguibile. Codice uscita: "+ str(res.returncode))
    except subprocess.CalledProcessError as e:
        print("Process exited with code: " + str(e.returncode))
    except Exception as ex:
        print("Generic error: " + str(ex))


data = cfn(GG_CORE_NAME)
data['stackName'] = str(hostname.stdout.strip()) + "-stack"
#install_greengrass(data)
create_stack(data)
