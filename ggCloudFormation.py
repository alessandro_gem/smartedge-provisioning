import sys
import time
import os
import traceback
import subprocess
import boto3
from botocore.exceptions import ClientError
import json
import logging


data = {"stackName": "GioE-187E49-stack", "thingName": "GioE-187E49", "certificateId": "2afad9f7ca8939e63157a5e65fdee10d8e1ee52177f39532e53c05bf22bd1f2f", "templateName": "greengrass_stack_lambda_ref.yml"}
functionName = "PlantDataIO"

'''
   Get Lambda Function arn given its name as input
'''
def get_function_arn(lambdaName):
    client = boto3.client('lambda')
    #print(functionName)
    function_info = client.get_function(FunctionName=functionName, Qualifier="Plant_Data_Sender")
    #print(function_info['Configuration']['FunctionArn'])
    return function_info['Configuration']['FunctionArn']

'''
   Greengrass Setup launcher function. When done, reboots the system to apply changes.
'''
def create_stack(data):
    client = boto3.client('cloudformation')
    data['functionArn'] = get_function_arn(functionName)
    params = [{'ParameterKey': 'GreengrassGroupName', 'ParameterValue': data['thingName']}, {'ParameterKey': 'CertificateId', 'ParameterValue': data['certificateId']}, {'ParameterKey': 'LambdaVersionArn', 'ParameterValue':data['functionArn']}]
    template = ""
    with open("greengrass_stack_lambda_ref.yml","r") as file:
        template = file.read()
    try:
        response = client.create_stack(StackName=data['stackName'], TemplateBody=template, Parameters=params, Capabilities=['CAPABILITY_IAM'])
        print(response)
    except subprocess.CalledProcessError as e:
        print("Process exited with code: " + str(e.returncode))
    except Exception as ex:
        print("Generic error: " + str(ex))
        traceback.print_exc()


create_stack(data)
