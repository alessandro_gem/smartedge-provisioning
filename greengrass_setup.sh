#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
CYAN='\033[0;36m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'
ROOT_FILE=/home/ggc_user/debug/root.ca.pem
GG_DIR=/greengrass
GGC_USER=/home/ggc_user
GG_ALREADY_INSTALLED=1
apt update -y
apt upgrade -y
if pip show greengrasssdk> /dev/null 2>&1; then
	echo "${CYAN}INFO - The greengrasssdk package is already installed.${NC}"
else
	if pip install greengrasssdk; then
		echo "${GREEN}SUCCESS - The greengrasssdk package is successfully installed.${NC}"
	else
		echo "${RED}FAIL - The greengrasssdk installation failed.${NC}"
	fi
fi

if [ ! -d "$GGC_USER" ]; then
	adduser --system ggc_user
	groupadd --system ggc_group
else
	echo "${CYAN}INFO - ggc_user and ggc_group already present.${NC}"
fi
cd /home/ggc_user

# Install Greengrass via APT repository (suitable for testing)
if dpkg -l aws-iot-greengrass-keyring> /dev/null 2>&1; then
	echo "${CYAN}INFO - The aws iot keyring package is already installed.${NC}"
else
	wget -O aws-iot-greengrass-keyring.deb https://d1onfpft10uf5o.cloudfront.net/greengrass-apt/downloads/aws-iot-greengrass-keyring.deb
	if dpkg -i aws-iot-greengrass-keyring.deb; then
		echo "${GREEN}SUCCESS - The aws iot keyring package is successfully installed.${NC}"
	else
		echo "${RED}FAIL - The aws iot keyring installation failed.${NC}"
	fi
fi

echo "deb https://dnw9lb6lzp2d8.cloudfront.net stable main" | sudo tee /etc/apt/sources.list.d/greengrass.list
apt update -y

if dpkg -l aws-iot-greengrass-core> /dev/null 2>&1; then
	echo "${CYAN}INFO - Greengrass core already installed.${NC}"
else
	if apt install aws-iot-greengrass-core -y; then
		echo "${GREEN}SUCCESS - Greengrass core successfully installed.${NC}"
	else
		echo "${RED}FAIL - Greengrass core installation failed.${NC}"
	fi
fi

if [ ! -d /greengrass ]; then 
	mkdir /greengrass
fi
#
# Greengrass configuration section
#
if [ ! -d /greengrass/certs ]; then
	cd /greengrass
	mkdir certs
else
	echo "${CYAN}INFO - The /greengrass/certs directory already present.${NC}"
fi

cd /home/ggc_user/debug

if [ ! -f /greengrass/certs/"$1".pem ]; then
	cp "$1".pem /greengrass/certs/
	echo "${GREEN}Pem Certificate file moved to /greengrass/certs${NC}"
else
	echo "${CYAN}INFO - Greengrass already stores pem.${NC}"
fi
if [ ! -f /greengrass/certs/"$1".key ]; then
	cp "$1".key /greengrass/certs/
	echo "${GREEN}Private key file moved to /greengrass/certs${NC}"
else
	echo "${CYAN}INFO - Greengrass already stores private key.${NC}"
fi


if [ ! -d /greengrass/config ]; then
	mkdir /greengrass/config
else
	cd /greengrass/config
fi

# Create Greengrass config file from inputs and parameters
# Can be encd /ghanced to manage complete installation of Greengrass and credentials
if [ ! -f "./config.json" ]; then
	cat <<EOT > config.json          
{
"coreThing" : {
  "caPath" : "root.ca.pem",
  "certPath" : "$1.pem",
  "keyPath" : "$1.key",
  "thingArn" : "$2",
  "iotHost" : "$3",
  "ggHost" : "greengrass-ats.iot.$4.amazonaws.com"
},
"runtime" : {
  "cgroup" : {
	"useSystemd" : "yes"
  }
},
"managedRespawn" : false,
"crypto" : {
  "principals" : {
	"SecretsManager" : {
	  "privateKeyPath" : "file:///greengrass/certs/$1.key"
	},
	"IoTCertificate" : {
	  "privateKeyPath" : "file:///greengrass/certs/$1.key",
	  "certificatePath" : "file:///greengrass/certs/$1.pem"
	}
  },
  "caPath" : "file:///greengrass/certs/root.ca.pem"
}
}
EOT
fi


cd /greengrass/certs/
if [ ! -f root.ca.pem ]; then
	if wget -O root.ca.pem https://www.amazontrust.com/repository/AmazonRootCA1.pem; then
		echo "${GREEN}SUCCESS - RootCA downloaded in /greengrass/certs${NC}"
	else
		echo "${RED}FAIL - RootCA download failed.${NC}"
	fi
fi
cd /tmp
if [ ! -f /etc/systemd/system/greengrass.service ]; then
	cat <<EOT > greengrass.service
[Unit]
Description=greengrass daemon
After=network.target

[Service]
ExecStart=/greengrass/ggc/core/greengrassd start
Type=simple
RestartSec=2
Restart=always
User=root
PIDFile=/var/run/greengrassd.pid

[Install]
WantedBy=multi-user.target
EOT
cp greengrass.service /etc/systemd/system
else
	cd /etc/systemd/system
fi
systemctl enable greengrass.service
echo "${CYAN}INFO - Configuration done.${NC}"
